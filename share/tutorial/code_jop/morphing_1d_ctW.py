import argparse
import os
import ROOT
import math
import numpy as np
from pprint import pprint

ROOT.gROOT.SetBatch(True)


def main():
    
    find_every_khi_squared = True
    fit_to_input = False
    
    file_name = "/morphing_samples_1d_withfit_to_input"
    #file_name = "test_with_NLO_samples"

    if fit_to_input:
        output_dir = os.getcwd()+"/plots/2021-06-1/morphing/fit_to_input"
    else:
        output_dir = os.getcwd()+"/plots/2021-06-1/morphing/"
    

    #this is the infilename for my own morphing_samples.root
    infilename = "/data/atlas/users/jopdej/analysis/samples/morphing_samples.root"
    observable = "cos_theta_x"

    validation_infilename = "/data/atlas/users/jopdej/analysis/samples/validation_samples.root"
    validation_samplelist = ["ctWI_c0_ctW_c-1" ,"ctWI_c0_ctW_c1","SM","POWHEG","SKIMM.ictW_c0_ctW_c1"]
    validation_observable = "cos_theta_x"
    #extra samples (NLO):  "ctWI_c0p5_ctW_c0p5","SKIMM.ictW_c0p526316_ctW_c-6p84211", "SKIMM.ictW_c1_ctW_c0",
    #extra samples (in input): 

    # validation_infilename = "/data/atlas/users/mdebeurs/Production/Morphing/MakeInput/v3_scaled.root"
    # validation_samplelist = ["ictW-c-0.3-ctW-c-3","ictW-c-10-ctW-c-10","ictW-c-10-ctW-c0.526316","ictW-c-10-ctW-c10","ictW-c-4.73684-ctW-c-10","ictW-c-4.73684-ctW-c1.57895","ictW-c-5.78947-ctW-c-5.78947","ictW-c0.526316-ctW-c-6.84211","ictW-c1.57895-ctW-c-10","ictW-c10-ctW-c-10","ictW-c10-ctW-c-4.73684","ictW-c10-ctW-c3.68421","ictW-c2.63158-ctW-c3.68421","ictW-c4.73684-ctW-c10","ictW-c6.84211-ctW-c-3.68421"]
    # validation_observable = "selection_cos_ly_part_top"
    
    #samplelist of LO_NLOPDF morphing_samples.root
    #samplelist = ["ctWI_c0_ctW_c1","SM","ctWI_c-10_ctW_c-10","ctWI_c-10_ctW_c10", "ctWI_c0p3_ctW_c3","ctWI_c-10_ctW_c0p526316" ,"ctWI_c-4p73684_ctW_c-10","ctWI_c-5p78947_ctW_c5p78947","ctWI_c0p526316_ctW_c-6p84211","ctWI_c10_ctW_c3p68421","ctWI_c1p57895_ctW_c-10","ctWI_c2p63158_ctW_c3p68421","ctWI_c4p73684_ctW_c10","ctWI_c6p84211_ctW_c3p68421","ctWI_c10_ctW_c-4p73684"]
    #extra samples: ,"ctWI_c1_ctW_c0","ctWI_c10_ctW_c-10",

    #samplelist of v3_thestat_withSM_val.root
    infilename = "/data/atlas/users/mdebeurs/Production/Morphing/MakeInput/v3_thestat_withSM_val.root"
    samplelist = ["ictW-c0-ctW-c0","ictW-c-10-ctW-c-10","ictW-c-10-ctW-c0.526316","ictW-c-10-ctW-c10"  , "ictW-c-4.73684-ctW-c-10","ictW-c-4.73684-ctW-c1.57895", "ictW-c-5.78947-ctW-c-5.78947", "ictW-c0.526316-ctW-c-6.84211","ictW-c1.57895-ctW-c-10","ictW-c10-ctW-c-10","ictW-c10-ctW-c-4.73684" ,      "ictW-c10-ctW-c3.68421","ictW-c2.63158-ctW-c3.68421",   "ictW-c4.73684-ctW-c10","ictW-c6.84211-ctW-c-3.68421"]
    observable = "selection_cos_ly_part_inc"

    #samplelist of v3_scaled.root
    #samplelist = ["ictW-c-0.3-ctW-c-3","ictW-c-10-ctW-c-10","ictW-c-10-ctW-c0.526316","ictW-c-10-ctW-c10","ictW-c-4.73684-ctW-c-10","ictW-c-4.73684-ctW-c1.57895","ictW-c-5.78947-ctW-c-5.78947","ictW-c0.526316-ctW-c-6.84211","ictW-c1.57895-ctW-c-10","ictW-c10-ctW-c-10","ictW-c10-ctW-c-4.73684","ictW-c10-ctW-c3.68421","ictW-c2.63158-ctW-c3.68421","ictW-c4.73684-ctW-c10","ictW-c6.84211-ctW-c-3.68421"]

    #Samplelist v3_morestat.root:
    # infilename = "/data/atlas/users/mdebeurs/Production/Morphing/MakeInput/v3_morestat.root"
    # samplelist = ["ictW-c-0.3-ctW-c-3","ictW-c-10-ctW-c-10","ictW-c-10-ctW-c0.526316","ictW-c-10-ctW-c10","ictW-c-4.73684-ctW-c-10","ictW-c-4.73684-ctW-c1.57895","ictW-c-5.78947-ctW-c-5.78947","ictW-c0.526316-ctW-c-6.84211","ictW-c1.57895-ctW-c-10","ictW-c10-ctW-c-10","ictW-c10-ctW-c-4.73684","ictW-c10-ctW-c3.68421","ictW-c2.63158-ctW-c3.68421","ictW-c4.73684-ctW-c10","ictW-c6.84211-ctW-c-3.68421"]
    # observable = "selection_cos_lx_part_top"

    #This is for checking the code with inputs as validations
    if fit_to_input:
        validation_infilename = infilename
        validation_samplelist = samplelist
        validation_observable = observable

    if infilename == "/data/atlas/users/mdebeurs/Production/Morphing/MakeInput/v3_thestat_withSM_val.root" or "/data/atlas/users/mdebeurs/Production/Morphing/MakeInput/v3_morestat.root":
        file_name = "/NLO"+file_name

    # push all the input samples in a RooArgList
    inputs = ROOT.RooArgList()
    # we need the additional list "inputnames" to prevent the python garbage collector from deleting the RooStringVars
    inputnames = []
    for sample in samplelist:
        print sample
        v = ROOT.RooStringVar(sample,sample,sample)
        inputnames.append(v)
        inputs.add(v)

    #setup morphfunc
    kSM = ROOT.RooRealVar("kSM","kSM",1.,0.,2.)
    ictW = ROOT.RooRealVar("ictW","ictW",0.,-10.,10.)
    ctW = ROOT.RooRealVar("ctW","ctW",0.,-10.,10.)
    NP0 = ROOT.RooRealVar("NP0","NP0",1.)

    #add a normalization factor?
    k = ROOT.RooRealVar("k","kappa", 0., 0., 100000.)

    gSM = ROOT.RooFormulaVar("_gSM", "kSM", ROOT.RooArgList(kSM))
    gictW = ROOT.RooFormulaVar("_gictW","ictW",ROOT.RooArgList(ictW))
    gctW = ROOT.RooFormulaVar("_gctW","ctW",ROOT.RooArgList(ctW))

    prodCouplings = ROOT.RooArgSet("Wtbp")
    decCouplings = ROOT.RooArgSet("Wtbd")

    prodCouplings.add(gSM)
    prodCouplings.add(gictW)
    prodCouplings.add(gctW)
    decCouplings.add(gSM)
    decCouplings.add(gictW)
    decCouplings.add(gctW)

    
    #morphexp = ROOT.RooLagrangianMorphFunc("morphfunc1","morphfunc1",infilename,observable,prodCouplings,decCouplings,inputs)
    print inputs
    morphexpr = ROOT.RooLagrangianMorphFunc("morphfunc","morphfunc",infilename,observable,prodCouplings,decCouplings,inputs)

    morphfunc = ROOT.RooLagrangianMorphPdf("morphfunc","morphfunc",infilename,observable,prodCouplings,decCouplings,inputs)
    x = morphfunc.getObservable()

    morphfunc.writeCoefficients("test.txt")
    morphfunc.printSampleWeights()
    morphfunc.printSampleWeights('test8.txt')

    f = open("test8.txt", "r")
    a = f.readlines()
    morphweightsdict = {}
    w = ROOT.RooWorkspace("w","workspace")

    for line in a:
        newline = line.split("=")
        function = newline[1].strip()
        function = function.replace("_gSM", "@0")
        function = function.replace("_gictW", "@1")
        function = function.replace("_gctW", "@2")
        function = function.replace("nNP0", "@3")
        morphweightsdict[newline[0].strip()] = function  
        print function
    f.close()
    
    #pprint(morphweightsdict)

    keys = morphweightsdict.keys()
    
    print keys
    #for key in keys:
    #    histweight = ROOT.RooFormulaVar(key, morphweightsdict[key], ROOT.RooArgList(kSM, ictW, ctW, NP0))
    #    getattr(w, "import")(histweight, ROOT.RooFit.RecycleConflictNodes())
    #    histweightslist.append(histweight)

    allroodict = {}
    binnumbers = 0

    
    roogaussians = []
    nuisanceparameter_rooset =ROOT.RooArgSet()
    global_observables =ROOT.RooArgSet()



    for i,sample in enumerate(samplelist):
        sampledict = {}
        roovarbin = []
        roobincontents = []
        samplecontributions = []
        roovarbinerror = []

        tfile = ROOT.TFile.Open(infilename,"READ")
        folder = tfile.Get(sample)
        folder.ls()
        hist_sample = folder.FindObject(observable)

        weightkey = "w_{}_morphfunc".format(sample)
        histweight = ROOT.RooFormulaVar(weightkey, morphweightsdict[weightkey], ROOT.RooArgList(kSM, ictW, ctW, NP0))
        getattr(w, "import")(histweight, ROOT.RooFit.RecycleConflictNodes())

        sampledict["histweightfunc"] = histweight
        
        if binnumbers != hist_sample.GetNbinsX() and i >0:
            print "your binnumbers changed from one input to another be very careful"

        binnumbers = hist_sample.GetNbinsX()
        for binnumber in range(binnumbers):
            
            bincontent = hist_sample.GetBinContent(binnumber+1)
            variablename = "{}_{}_bin{}".format(sample, observable, binnumber+1)
            bincontentname = "bincontent_{}_{}_bin{}".format(sample, observable, binnumber+1)

            roovar = ROOT.RooRealVar(variablename,variablename,bincontent, 0, ROOT.RooNumber.infinity())
            roobincontent = ROOT.RooRealVar(bincontentname,bincontentname,bincontent)
            roovarbin.append(roovar)
            roobincontents.append(roobincontent)
            getattr(w, "import")(roovar, ROOT.RooFit.RecycleConflictNodes())
            getattr(w, "import")(roobincontent, ROOT.RooFit.RecycleConflictNodes())


            samplecontribution_name = "weighted_{}_{}_bin{}".format(sample, observable, binnumber+1)
            samplecontribution= ROOT.RooFormulaVar(samplecontribution_name, "@0*@1", ROOT.RooArgList(histweight,roovar))
            getattr(w, "import")(samplecontribution, ROOT.RooFit.RecycleConflictNodes())
            samplecontributions.append(samplecontribution)

            binerror = hist_sample.GetBinError(binnumber+1)
            errorname = "error_{}_{}_bin{}".format(sample, observable, binnumber+1)
            rooerror = ROOT.RooConstVar(errorname,errorname,binerror)
            roovarbinerror.append(rooerror)

            gausname = "constrain_{}".format(variablename) 
            #make the constrain on the bincontent as roogaus(bincontentnom|roovarbin, roovarbinerror)
            constrain = ROOT.RooGaussian(gausname, gausname, roobincontent,roovar, rooerror)
            roogaussians.append(constrain)
            nuisanceparameter_rooset.add(roovar)
            global_observables.add(roobincontent)
            getattr(w, "import")(rooerror, ROOT.RooFit.RecycleConflictNodes())
            getattr(w, "import")(constrain, ROOT.RooFit.RecycleConflictNodes())


        
        sampledict["roovarbin"] = roovarbin
        sampledict["roobincontent"] = roobincontents
        sampledict["samplecontributions"] = samplecontributions
        sampledict["roovarbinerror"] = roovarbinerror


        allroodict[sample] = sampledict


        #param_card = folder.FindObject("param_card")
        tfile.Close()
    pprint(allroodict)

    #ToDo might change the final mu to the one with all the nuisances included if I got that
    mutjes = ROOT.RooArgList()
    mutjeslist = [] #because roofit is stupid you have to save the object in a list otherwise mubin gets destroyed
    binexpression=[]
    for binnumber in range(binnumbers):
        binexpression.append("@{}".format(binnumber))
        weightedcontribution_samples = []
        expression = ""
        _expressions = []
        _rooarglist = ROOT.RooArgList()
        for i,sample in enumerate(samplelist):
            _rooarglist.add(allroodict[sample]['samplecontributions'][binnumber])
            _expressions.append( "@{}".format(i))
        expression = "+".join(_expressions)
        _rooarglist.Print()
        muname = "mu_bin{}".format(binnumber+1)

        mubin = ROOT.RooFormulaVar(muname, expression, _rooarglist)
        getattr(w, "import")(mubin, ROOT.RooFit.RecycleConflictNodes())
        mutjeslist.append(mubin)
        mutjes.add(mubin)
    
    normfunc = "+".join(binexpression)
    normalization = ROOT.RooFormulaVar("Normalization", normfunc, mutjes)
    getattr(w, "import")(normalization, ROOT.RooFit.RecycleConflictNodes())
   


    mubinnormlist = []
    for binnumber in range(binnumbers):
        mubinnorm = ROOT.RooFormulaVar("norm_mu_bin{}".format(binnumber+1), "@0/@1", ROOT.RooArgList(mutjeslist[binnumber], normalization))
        mubinnormlist.append(mubinnorm)
        getattr(w, "import")(mubinnorm, ROOT.RooFit.RecycleConflictNodes())

    
    cov_matrix = ROOT.TMatrixDSym(7)

    validation_sample = samplelist[0] #go to the sm

    tfile = ROOT.TFile.Open(infilename,"READ")
    folder = tfile.Get(validation_sample)
    validation = folder.FindObject(observable)
    validation.Scale(1./validation.Integral())
    

    validation_bins = []

    for binnumber_val in range(validation.GetNbinsX()):
        print binnumber_val
        bincontent = validation.GetBinContent(binnumber_val+1)
        print bincontent
        variablename = "validation_{}_bin{}".format(observable, binnumber_val+1)
        roovar = ROOT.RooRealVar(variablename,variablename,bincontent)
        validation_bins.append(roovar)
        getattr(w, "import")(roovar, ROOT.RooFit.RecycleConflictNodes())

        #samplecontribution_name = "weighted_{}_{}_bin{}".format(sample, observable, binnumber+1)
        #samplecontribution= ROOT.RooFormulaVar(samplecontribution_name, "@0*@1", ROOT.RooArgList(histweight,roovar))
        #getattr(w, "import")(samplecontribution, ROOT.RooFit.RecycleConflictNodes())
        #samplecontributions.append(samplecontribution)

        _binerror = validation.GetBinError(binnumber_val+1)
        print "binerror", _binerror
        if binnumber_val ==validation.GetNbinsX():
            pass
        else:
            cov_matrix[binnumber_val, binnumber_val] = _binerror
        #errorname = "error_validation_{}_bin{}".format( observable, binnumber_val+1)
        #rooerror = ROOT.RooConstVar(errorname,errorname,binerror)
        #roovarbinerror.append(rooerror)
        #getattr(w, "import")(rooerror, ROOT.RooFit.RecycleConflictNodes())

    mu_vect = ROOT.RooArgList()
    x_vect = ROOT.RooArgList()
    print "initializing x_data"
    dummyx = ROOT.RooRealVar("dummy","dummy",0, 1)
    #x_data.SetName("asimov")

    for i in range(7):
        mu_vect.add(mubinnormlist[i])
        x_vect.add(validation_bins[i])
        print validation_bins[i]
        #x_data.addColumn(ROOT.RooArgSet(validation_bins[i]))
        #x_data.add(ROOT.RooArgSet(validation_bins[i]))#you need a dataset for the fit which is a bit stupid but well ok
    x_data = ROOT.RooDataSet("asimov", "asimov", ROOT.RooArgSet(x_vect))
    x_data.add(ROOT.RooArgSet(x_vect))

    

    normed_multivarpdf = ROOT.RooMultiVarGaussian("normed_multivarpdf","normed_multivarpdf",x_vect, mu_vect, cov_matrix)
    getattr(w, "import")(normed_multivarpdf, ROOT.RooFit.RecycleConflictNodes())
    getattr(w, "import")(cov_matrix)
    
    roogaussians_roolist = list_to_roolist([normed_multivarpdf], roogaussians)
    finalpdf = ROOT.RooProdPdf("completepdf", "completepdf", roogaussians_roolist)   
    getattr(w, "import")(finalpdf, ROOT.RooFit.RecycleConflictNodes())


    getattr(w, "import")(x_data, ROOT.RooFit.RecycleConflictNodes())

    modelconfig = ROOT.RooStats.ModelConfig("ModelConfig",w)

    modelconfig.SetPdf(finalpdf)


    pois = ROOT.RooArgSet(kSM,ictW, ctW)
    x_set = ROOT.RooArgSet(x_vect)

    all_params = ROOT.RooArgSet(pois, nuisanceparameter_rooset)
    #all_params = ROOT.RooArgSet(all_params, ROOT.RooArgSet(kSM))
    w.saveSnapshot("nominalNuis", nuisanceparameter_rooset)

    modelconfig.SetParametersOfInterest(pois)
    modelconfig.SetObservables((x_set)) 
    modelconfig.SetNuisanceParameters(nuisanceparameter_rooset)
    modelconfig.SetGlobalObservables(global_observables)
    modelconfig.SetSnapshot(pois)



    getattr(w, "import")(modelconfig)

    w.writeToFile("test_workspace_y.root")


    
    #ToDo: 
    #       make a covariance matrix on unnormalised dinges
    #       use jacobian to transform to normalised
    #       make multivargaus
    #       multiply with the alpha params of the MC stat

    print 
    print 
    print
    print 
    w.Print()
    x_data.Print()
    cov_matrix.Print()



def list_to_roolist(*args):
    if args == None:
        print 
        raise RuntimeError("no lists to make, I stop since this will probably crash further down the code")
    roolist = ROOT.RooArgList()
    for arg in args:
        for entry in arg:
            roolist.add(entry)
    return roolist
        


if __name__ == "__main__":
  # some argument parsing to provide the path to the RooLagrangianMorphFunc
  parser = argparse.ArgumentParser("morphing example")
  args = parser.parse_args()
  # load all required libraries
  from ROOT import gSystem
  #ROOT.gInterpreter.ProcessLine('#include "/data/atlas/users/jdegens/analysis/Morphing/RooLagrangianMorphing/RooLagrangianMorphing/RooLagrangianMorphing.h"')

  gSystem.Load("libRooFit")
  gSystem.Load("libRooLagrangianMorphing.so")
  # call the main function
  main()