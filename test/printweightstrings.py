import ROOT
ROOT.gROOT.SetBatch(True)
ROOT.gSystem.Load("libRooFit")
ROOT.gSystem.Load("libHistFactory")
ROOT.gSystem.Load("libRooLagrangianMorphing.so")

String = ROOT.string
VecString = ROOT.vector(ROOT.string)
Double = ROOT.double
VecVecString = ROOT.vector(VecString)
ParamSet = ROOT.RooLagrangianMorphing.ParamSet
ParamMap = ROOT.RooLagrangianMorphing.ParamMap

def main():
    couplings = VecString()
    couplings.push_back(String("SM"))
    couplings.push_back(String("dtilde"))    
    vertices = VecVecString()
    vertices.push_back(couplings)
    
    inputs = ParamMap()
    for valtuple in [(1,0),(1,1),(0,1)]:
        paramvalues = ParamSet()
        namecomponents = []
        for idx in range(0,len(valtuple)):
            ROOT.RooLagrangianMorphing.append(paramvalues,couplings[idx],valtuple[idx])
            namecomponents.append("{:s}_{:f}".format(couplings[idx],valtuple[idx]))
            name="_".join(namecomponents)
        ROOT.RooLagrangianMorphing.append(inputs,name,paramvalues)

    weightstrings = ROOT.RooLagrangianMorphing.createWeightStrings(inputs,vertices)

    for w,s in weightstrings:
        print("{:s} = {:s}".format(w,s))
        

if __name__=="__main__":
    main()
