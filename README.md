# RooLagrangianMorphing

RooLagrangianMorphing is a standalone RooFit implementation of the
method of Effective Lagrangian Morphing, descibed in
[ATL-PHYS-PUB-2015-047](https://cds.cern.ch/record/2066980). Effective
Lagrangian Morphing is a method to construct a continuous signal model
in the coupling parameter space. Basic assumption is that shape and
cross section of a physical distribution is proportional to it's
squared matrix element. The signal model is constructed by a weighted
sum over _N_ input distributions. The calculation of the weights is
based on Matrix Elements evaluated for the different input scenarios.

The number of input files depends on the number of couplings in
production and decay vertices, and also whether the decay and
production vertices describe the same process or not.

While the RooLagrangianMorphing implementation in principle supports
arbitrary effective lagrangian models, a few specific derived classes
are available to provide increased convenience for use with the
[Higgs Characterisation Model](http://feynrules.irmp.ucl.ac.be/wiki/HiggsCharacterisation) as well as the [SMEFT model](https://feynrules.irmp.ucl.ac.be/wiki/SMEFT).

## Try on SWAN
[![text](http://swanserver.web.cern.ch/swanserver/images/badge_swan_white_150.png)](https://cern.ch/swanserver/cgi-bin/go/?projurl=https://github.com/vincecr0ft/MorphingTutorials.git)
Before running anything you must compile the libraries so that SWAN can find them. To do this open a terminal (`>_` symbol on the top right) `cd SWAN_projects/MorphingTutorial/build` then run `source build.sh` then restart any running kernels.

## Installation

After downloading the package, you can compile it using

    mkdir build
    cd build
    cmake ..
    make -j4

The package supports arbitrary precision numerics using
boost_multiprecision and boost_uBLAS. These features introduce
header-only compile-time dependency on the boost package. If no
suitable boost version is available, you can use the following one
deposited on AFS

    cmake .. -DBoost_NO_SYSTEM_PATHS=TRUE -DBOOST_ROOT:PATHNAME=/afs/cern.ch/user/c/cburgard/public/boost-1.59/
